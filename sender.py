import time
import socket

# UDP_IP = "127.0.0.1"
UDP_PORT = 5005
MESSAGE1 = "lcd_setup"
MESSAGE2 = "lcd_backlight_on"
MESSAGE3 = "lcd_backlight_off"
MESSAGE4 = "close_server"

MESSAGE1 = "pnt_setup"
MESSAGE2 = "pnt_tilt_up"
MESSAGE3 = "pnt_tilt_down"
MESSAGE4 = "close_server"

# UDP_IP = socket.gethostbyname(socket.gethostname())
UDP_IP = "192.168.1.82"

print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT
# print "message:", MESSAGE

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.sendto(MESSAGE1, (UDP_IP, UDP_PORT))
time.sleep(1)
sock.sendto(MESSAGE2, (UDP_IP, UDP_PORT))
time.sleep(1)
sock.sendto(MESSAGE3, (UDP_IP, UDP_PORT))
time.sleep(1)
sock.sendto(MESSAGE4, (UDP_IP, UDP_PORT))
