#!/usr/bin/python
# -*- coding: iso-8859-1 -*-

import Tkinter

# class of a Tk GUI, inherit from base class Tkinter.Tk
class clTkSender(Tkinter.Tk):
    def __init__(self,parent):
        # call base class constructor
        Tkinter.Tk.__init__(self,parent)
        # keep a local reference to the parent 
        self.parent = parent
        # run GUI initialize
        self.initialize()

    def initialize(self):
        # our gui will use the layout grid manager (inherited from Tk)
        self.grid()
        
        self.entryVariable = Tkinter.StringVar()
        # create an entry widget
        self.entry = Tkinter.Entry(self,textvariable=self.entryVariable)
        # add it to the grid
        self.entry.grid(column=0,row=0,sticky='EW')
        # bind the object to the <Return> key
        self.entry.bind("<Return>", self.OnPressEnter)
        self.entryVariable.set(u"Enter text here.")

        # create a button & bind it to a method
        button = Tkinter.Button(self,text=u"Click me !", command=self.OnButtonClick)
        # add it to the grid
        button.grid(column=1,row=0)

        # create the label
        self.labelVariable = Tkinter.StringVar()
        label = Tkinter.Label(self,textvariable=self.labelVariable, anchor="w",fg="white",bg="blue")
        # add it to the grid
        label.grid(column=0,row=1,columnspan=2,sticky='EW')
        # default value
        self.labelVariable.set(u"Hello !")

        # resize options
        self.grid_columnconfigure(0,weight=1)
        self.resizable(True,False)
        
        # lock geometry
        self.update()
        self.geometry(self.geometry())

        # default focus set
        self.entry.focus_set()
        self.entry.selection_range(0, Tkinter.END)

    def OnButtonClick(self):
        # command handler for button
        self.labelVariable.set( self.entryVariable.get()+" (You clicked the button)" )
        # default focus set
        self.entry.focus_set()
        self.entry.selection_range(0, Tkinter.END)

    def OnPressEnter(self,event):
        # command handler for enter key
        self.labelVariable.set( self.entryVariable.get()+" (You pressed ENTER)" )
        # default focus set
        self.entry.focus_set()
        self.entry.selection_range(0, Tkinter.END)

if __name__ == "__main__":
    # create an instance of a GUI
    app = clTkSender(None)
    # add a title
    app.title('RPi Commander')
    # run the blocking loop checking for GUI input
    app.mainloop()