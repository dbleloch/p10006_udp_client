import socket
import Tkinter

# class of a Tk GUI, inherit from base class Tkinter.Tk
class clTkSender(Tkinter.Tk):

    def __init__(self,parent):
        # set up some class constants
        self.UDP_PORT = 5005
        self.UDP_IP = "192.168.1.82"
        self.MESSAGE_SETUP = "pnt_setup"
        self.MESSAGE_UP    = "pnt_tilt_up"
        self.MESSAGE_DOWN  = "pnt_tilt_down"
        self.MESSAGE_LEFT  = "pnt_pan_left"
        self.MESSAGE_RIGHT = "pnt_pan_right"
        self.MESSAGE_CLOSE = "close_server"
        # create an open socket belonging to the instance of class TkSender
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        # call base class constructor
        Tkinter.Tk.__init__(self,parent)
        # keep a local reference to the parent 
        self.parent = parent
        # run GUI initialize
        self.initialize()

    def initialize(self):
        # our gui will use the layout grid manager (inherited from Tk)
        self.grid()

        # create buttons & bind it to methods
        ####SETUP####
        buttonSetup = Tkinter.Button(self,text=u"SETUP", command=self.OnButtonClickSetup)
        # add it to the grid
        buttonSetup.grid(column=1,row=0)
        ####UP####
        buttonUp = Tkinter.Button(self,text=u"UP", command=self.OnButtonClickUp)
        # add it to the grid
        buttonUp.grid(column=1,row=2)
        ###LEFT###
        buttonLeft = Tkinter.Button(self,text=u"LEFT", command=self.OnButtonClickLeft)
        # add it to the grid
        buttonLeft.grid(column=0,row=3)
        ###RIGHT###
        buttonRight = Tkinter.Button(self,text=u"RIGHT", command=self.OnButtonClickRight)
        # add it to the grid
        buttonRight.grid(column=2,row=3)
        ###DOWN###
        buttonDown = Tkinter.Button(self,text=u"DOWN", command=self.OnButtonClickDown)
        # add it to the grid
        buttonDown.grid(column=1,row=4)
        
        # create the label
        self.labelVariable = Tkinter.StringVar()
        label = Tkinter.Label(self,textvariable=self.labelVariable, anchor="w",fg="white",bg="blue")
        # add it to the grid
        label.grid(column=0,row=5,columnspan=3,sticky='EW')
        # default value
        self.labelVariable.set(u"Ready")

        # resize options
        self.grid_columnconfigure(0,weight=1)
        self.resizable(False,False)
        
        # lock geometry
        self.update()
        self.geometry(self.geometry())
        
    def OnButtonClickSetup(self):
        # command handler for button
        self.labelVariable.set( "<<<SETUP>>>" )
        # send a message to the socket
        self.sock.sendto(self.MESSAGE_SETUP, (self.UDP_IP, self.UDP_PORT))
        return True

    def OnButtonClickUp(self):
        # command handler for button
        self.labelVariable.set( "<<<UP>>>" )
        # send a message to the socket
        self.sock.sendto(self.MESSAGE_UP, (self.UDP_IP, self.UDP_PORT))

    def OnButtonClickDown(self):
        # command handler for button
        self.labelVariable.set( "<<<DOWN>>>" )
        # send a message to the socket
        self.sock.sendto(self.MESSAGE_DOWN, (self.UDP_IP, self.UDP_PORT))

    def OnButtonClickLeft(self):
        # command handler for button
        self.labelVariable.set( "<<<LEFT>>>" )
        # send a message to the socket
        self.sock.sendto(self.MESSAGE_LEFT, (self.UDP_IP, self.UDP_PORT))
        
    def OnButtonClickRight(self):
        # command handler for button
        self.labelVariable.set( "<<<RIGHT>>>" )
        # send a message to the socket
        self.sock.sendto(self.MESSAGE_RIGHT, (self.UDP_IP, self.UDP_PORT))

if __name__ == "__main__":
    
    # create an instance of a GUI
    app = clTkSender(None)
    # run the blocking loop checking for GUI input
    app.title("")
    app.mainloop()